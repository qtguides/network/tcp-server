#ifndef SERVER_H
#define SERVER_H

#include <QTcpSocket>
#include <QTcpServer>

class Server : public QTcpServer
{
    Q_OBJECT

private:
    QTcpSocket * socket;

public:
    explicit Server(QObject *parent = nullptr);
    void enviar(const QString &mensaje);

signals:

public slots:
    void conectarCliente();
};

#endif // SERVER_H
