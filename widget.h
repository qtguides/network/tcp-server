#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "server.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

private:
    Server server;

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_cerrar_clicked();

    void on_iniciar_clicked();

    void on_enviar_clicked();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
