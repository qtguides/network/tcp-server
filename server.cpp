#include "server.h"

Server::Server(QObject *parent)
    : QTcpServer(parent)
{
    socket = nullptr;
    connect(this, &Server::newConnection, this, &Server::conectarCliente);
}

void Server::conectarCliente()
{
    socket = nextPendingConnection();
}

void Server::enviar(const QString &mensaje)
{
    if(socket)
    {
        QTextStream stream(socket);
        stream << mensaje;
        socket->flush();
    }
}
