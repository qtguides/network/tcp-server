#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    server(this),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_cerrar_clicked()
{
    close();
}

void Widget::on_iniciar_clicked()
{
    bool status;
    uint16_t puerto = static_cast<uint16_t>(ui->puerto->value());

    status = server.listen(QHostAddress::Any, puerto);
    if(status)
        QMessageBox::information(this, "Servidor", "Servidor iniciado");
    else
        QMessageBox::critical(this, "Servidor", "Error iniciando servidor");

}

void Widget::on_enviar_clicked()
{
    server.enviar(ui->mensaje->text());
}
